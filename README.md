Step by step instruction
========================

1. git clone git@bitbucket.org:AlexMurm/lantern.git
2. cd lantern
3. virtualenv venv
4. source venv/bin/activate
5. pip install -r requirements.txt
6. python lantern.py

Python version - 2.7