#!/usr/bin/env python

import tornado.ioloop
from tornado import gen
from tornado.tcpclient import TCPClient
import sys
import struct

def get_ip_port_from_user_input():
    ip = raw_input("server ip address (default - 127.0.0.1): ")
    port = raw_input("server port (default - 9999): ")
    if ip == '':
        ip = '127.0.0.1'
    if port == '':
        port = 9999
    return ip, port

class Lantern(object):

    def __init__(self,ip,port):
        self.ip = ip
        self.port = port
        # Initiate new tcp client
        self.tcpclient = TCPClient()
        # Get event loop for non-blocking sockets
        self.ioloop = tornado.ioloop.IOLoop.instance()
        #Lantern state: 'ON' or 'OFF'
        self.state = 'OFF'
        #RGB color
        self.color = (0x00,0x00,0x00)
        # Create connection and start listening commands
        self.listen_commands()

    def start(self):
        # Start main event loop
        self.ioloop.start()

    @gen.coroutine
    def listen_commands(self):
        # Connect to the server
        try:
            self.stream = yield self.tcpclient.connect(host=self.ip,port=self.port)
        except Exception, e:
            if getattr(e,'errno',None):
                print "Error occurred. Error number is %d." % e.errno
            else:
                print e
            self.ioloop.stop()
            exit()
        print "===Successfully connected to the server %s:%s.===" % (self.ip, str(self.port))
        while True:
            try:
                yield self.read_bytes()
            except:
                print sys.exc_info()[0]
                self.ioloop.stop()
                exit()

    @gen.coroutine
    def read_bytes(self):
        type_length_bytes = yield self.stream.read_bytes(3)
        word_type, word_len = struct.unpack('>BH',type_length_bytes)
        if word_type == 0x12:
            self.state = 'ON'
            command = 'ON'
        elif word_type == 0x13:
            self.state = 'OFF'
            command = 'OFF'
        elif word_type == 0x20:
            color_bytes = yield self.stream.read_bytes(word_len)
            self.color = struct.unpack('>%iB'%word_len, color_bytes)
            command = 'COLOR'
        else:
            raise gen.Return()
        print "Got command '%s'. The lantern is %s. Color is (%02x,%02x,%02x)" % ((command,self.state,) + self.color)

if __name__ == '__main__':
    # Get ip address and port number of the server
    ip, port = get_ip_port_from_user_input()
    # Initiate Lantern instance; ip - server ip address, port - server port number
    lantern = Lantern(ip,port)
    # Start listening server
    lantern.start()